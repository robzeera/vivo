CREATE OR REPLACE PACKAGE BODY pac_rac IS

-- **************************************************************************************************
--  get_queue_key
-- **************************************************************************************************

FUNCTION get_queue_key(queue_name in varchar2) RETURN NUMBER
         as
      queue_key number;

      cursor c1 is
      SELECT max(r.RESOURCE_KEY) from resource_ r
      where r.resource_name = queue_name and r.GMT_END_TIME is null;

BEGIN
  open c1;
  fetch c1 into queue_key;

  if queue_key is null then
    queue_key := -2;
  end if;

  close c1;

  RETURN queue_key;

END get_queue_key;

-- **************************************************************************************************
--  pro_rac_analitico
-- **************************************************************************************************

PROCEDURE pro_rac_analitico
   (
      p_start_range   IN NUMBER
     ,p_end_range     IN NUMBER
   ) IS
      --
      v_min_std NUMBER := p_start_range /*- 1800*/;
      v_max_std NUMBER := p_end_range /*+1800*/;

      --
   BEGIN
      --
      pac_log.info(pac_name => $$PLSQL_UNIT, procedure_name => 'PRO_RAC_ANALITICO', message => 'INICIO DA PROCEDURE');
      --

      pac_rac.pro_rac_tmp(v_min_std, v_max_std);

      insert into AG2_RAC_ANALITICO(
              "DATE_TIME_KEY",
              "HORA",
              "INTERACTION_ID",
              "QUEUE_RESOURCE_KEY",
              "IRF_USER_DATA_CUST_1_KEY",
              "USER_DATA_CUST_DIM_1_KEY",
              "USER_DATA_CUST_DIM_3_KEY",
              "USER_DATA_CUST_DIM_13_KEY",
              "USER_DATA_CUST_DIM_14_KEY"
              )
      SELECT
            dt.date_time_key DATE_TIME_KEY,
            to_char(ts_to_date(irf.start_ts), 'HH24:MI:SS') HORA,
            irf.interaction_id INTERACTION_ID,
            CASE WHEN PAC_RAC.get_queue_key(iudc.VQ_RAC_IM8) = -2 THEN
               vq.resource_key
               ELSE PAC_RAC.get_queue_key(iudc.VQ_RAC_IM8)
            END QUEUE_KEY,
            iudc.interaction_resource_id IRF_USER_DATA_CUST_1_KEY,
            udcd1.ID USER_DATA_CUST_DIM_1_KEY,
            udcd3.ID USER_DATA_CUST_DIM_3_KEY,
            udcd13.ID USER_DATA_CUST_DIM_13_KEY,
            udcd14.ID USER_DATA_CUST_DIM_14_KEY

        FROM
            gim81.interaction_resource_fact irf
            inner join gim81.date_time dt on dt.date_time_key = irf.start_date_time_key and dt.date_time_key BETWEEN v_min_std AND (v_max_std-1)
            inner join tmp_rac irf_or on irf_or.interaction_resource_id = irf.interaction_resource_id
            inner join gim81.resource_ vq on vq.resource_key = irf.last_vqueue_resource_key
            inner join gim81.resource_ rp on rp.resource_key = irf.last_rp_resource_key
            inner join gim81.interaction_type it on it.interaction_type_key = irf.interaction_type_key
            inner join gim81.irf_user_data_cust_1 iudc on iudc.interaction_resource_id = irf.interaction_resource_id and iudc.start_date_time_key = irf.start_date_time_key
            inner join gim81.irf_user_data_keys iudk on iudk.interaction_resource_id = irf.interaction_resource_id and iudk.start_date_time_key = irf.start_date_time_key
            inner join gim81.USER_DATA_CUST_DIM_1 udcd1 on udcd1.id = iudk.custom_key_1
            inner join gim81.user_data_cust_dim_3 udcd3 on udcd3.id = iudk.custom_key_3
            inner join gim81.user_data_cust_dim_13 udcd13 on iudk.custom_key_13 = udcd13.id
            inner join gim81.user_data_cust_dim_14 udcd14 on iudk.custom_key_14 = udcd14.id
         where 1=1
            and vq.resource_name not in ('VQ_CallCenter','NONE')
            and it.interaction_type_code = 'INBOUND'
            and udcd13.u_rac_tipo not in ('none');

      --
      pac_log.info(pac_name => $$PLSQL_UNIT, procedure_name => 'PRO_RAC_ANALITICO', message => 'FIM DA PROCEDURE');
      --
   EXCEPTION
      WHEN no_data_found THEN
         pac_log.error(pac_name => $$PLSQL_UNIT, procedure_name => 'PRO_RAC_ANALITICO', message => 'NAO EXISTEM DADOS A SEREM AGREGADOS', code => SQLCODE, errm => SQLERRM);
         RAISE;
      WHEN storage_error THEN
         pac_log.error(pac_name => $$PLSQL_UNIT, procedure_name => 'PRO_RAC_ANALITICO', message => 'SEM MEMORIA DISPONIVEL OU MEMORIA CORROMPIDA', code => SQLCODE, errm => SQLERRM);
         RAISE;
      WHEN dup_val_on_index THEN
         pac_log.error(pac_name => $$PLSQL_UNIT, procedure_name => 'PRO_RAC_ANALITICO', message => 'TENTATIVA DE INSERCAO DE DADOS COM CHAVE DUPLICADA', code => SQLCODE, errm => SQLERRM);
         RAISE;
   END pro_rac_analitico;


-- **************************************************************************************************
--  pro_day_agg
-- **************************************************************************************************
PROCEDURE pro_day_agg
   (
      p_start_range   IN NUMBER
     ,p_end_range     IN NUMBER
   ) IS

      v_min_std NUMBER := p_start_range;
      v_max_std NUMBER := p_end_range;

  BEGIN

      pac_log.info(pac_name => $$PLSQL_UNIT, procedure_name => 'PRO_DAY_AGG', message => 'INICIO DA PROCEDURE');

       insert into AG2_RAC_ANALITICO_CONSOLIDADO(
              "DATE_TIME_KEY",
              "QUEUE_KEY",
              "TIPO_INCENTIVO",
              "ACEITE",
              "SEGMENTO",
              "PEDRA",
              "TIPO_CLIENTE",
              "URA",
              "OFERECIDAS",
              "TERMINAIS_UNICOS"
              )
            select
               dt.date_time_day_key DATE_TIME_KEY,
               CASE WHEN PAC_RAC.get_queue_key(iudc.VQ_RAC_IM8) = -2 THEN
                  vq.resource_key
                  ELSE PAC_RAC.get_queue_key(iudc.VQ_RAC_IM8)
               END QUEUE_KEY,
               case
                  when udcd13.u_rac_tipo ='0' then udcd13.u_rac_tipo_regra
                  when udcd13.u_rac_tipo_regra in ('ATF', 'Parametrizada') then udcd13.u_rac_tipo_regra
                  when substr(udcd13.u_rac_tipo_regra,1,4)='Fixa' then udcd13.u_rac_tipo_regra
                  when udcd13.u_rac_tipo ='1' and udcd13.s_rac_cod_que in ('RAC_1', 'RAC_3') and udcd13.s_rac_cod_abn ='NULL' then 'Verbalizacao'
                  when (udcd13.u_rac_tipo ='2' and udcd13.u_rac_tipo_regra <>'ATF') or udcd13.s_rac_cod_que = 'RAC_4' then 'Incentivo'
                  when udcd13.u_rac_tipo ='3' and udcd13.s_rac_cod_que in ('RAC_1', 'RAC_3') and udcd13.s_rac_cod_abn in ('NULL', 'RAC_2') then 'Verbalizacao+Incentivo'
                  when udcd13.u_rac_tipo_regra in ('NA') then udcd13.u_rac_tipo_regra
                  else udcd13.u_rac_tipo
               end TIPO_INCENTIVO,
               case
                  when udcd13.s_rac_op_sms = '1' then 'Aceite'
                  when udcd13.s_rac_op_sms = '2' then 'Não aceite'
                  when udcd13.s_rac_op_sms = 'timeout' then 'Sem resposta'
                  when udcd13.s_rac_op_sms = 'erro' then 'Falha  - Telefonia'
                  else udcd13.s_rac_op_sms
               end ACEITE,
               udcd3.CUSTOMER_SEGMENT SEGMENTO,
               udcd1.PEDRA PEDRA,
               udcd3.SERVICE_TYPE TIPO_CLIENTE,
               udcd14.VQ_POP URA,
               count(ira.interaction_id) OFERECIDAS,
               count(distinct iudc.account_number_ivr) AS TERMINAIS_UNICOS

            FROM VW_ITX_RAC_ANALITICO ira
            inner join gim81.date_time dt on dt.date_time_key = ira.date_time_key and dt.date_time_key BETWEEN v_min_std AND (v_max_std-1)
            inner join gim81.resource_ vq on vq.resource_key = ira.QUEUE_KEY
            inner join gim81.irf_user_data_cust_1 iudc on iudc.interaction_resource_id = ira.IRF_USER_DATA_CUST_1_KEY and iudc.start_date_time_key = ira.date_time_key
            inner join gim81.user_data_cust_dim_13 udcd13 on ira.USER_DATA_CUST_DIM_13_KEY = udcd13.id
            inner join gim81.user_data_cust_dim_14 udcd14 on ira.USER_DATA_CUST_DIM_14_KEY = udcd14.id
            inner join gim81.user_data_cust_dim_3 udcd3 on udcd3.id = ira.USER_DATA_CUST_DIM_3_KEY
            inner join gim81.USER_DATA_CUST_DIM_1 udcd1 on udcd1.id = ira.USER_DATA_CUST_DIM_1_KEY

            where 1=1

            group by dt.date_time_day_key,
            CASE WHEN PAC_RAC.get_queue_key(iudc.VQ_RAC_IM8) = -2 THEN
               vq.resource_key
               ELSE PAC_RAC.get_queue_key(iudc.VQ_RAC_IM8)
            END,
            case
               when udcd13.u_rac_tipo ='0' then udcd13.u_rac_tipo_regra
               when udcd13.u_rac_tipo_regra in ('ATF', 'Parametrizada') then udcd13.u_rac_tipo_regra
               when substr(udcd13.u_rac_tipo_regra,1,4)='Fixa' then udcd13.u_rac_tipo_regra
               when udcd13.u_rac_tipo ='1' and udcd13.s_rac_cod_que in ('RAC_1', 'RAC_3') and udcd13.s_rac_cod_abn ='NULL' then 'Verbalizacao'
               when (udcd13.u_rac_tipo ='2' and udcd13.u_rac_tipo_regra <>'ATF') or udcd13.s_rac_cod_que = 'RAC_4' then 'Incentivo'
               when udcd13.u_rac_tipo ='3' and udcd13.s_rac_cod_que in ('RAC_1', 'RAC_3') and udcd13.s_rac_cod_abn in ('NULL', 'RAC_2') then 'Verbalizacao+Incentivo'
               when udcd13.u_rac_tipo_regra in ('NA') then udcd13.u_rac_tipo_regra
               else udcd13.u_rac_tipo
            end,
            case
               when udcd13.s_rac_op_sms = '1' then 'Aceite'
               when udcd13.s_rac_op_sms = '2' then 'Não aceite'
               when udcd13.s_rac_op_sms = 'timeout' then 'Sem resposta'
               when udcd13.s_rac_op_sms = 'erro' then 'Falha  - Telefonia'
               else udcd13.s_rac_op_sms
            end,
            udcd3.CUSTOMER_SEGMENT,
            udcd1.PEDRA,
            udcd3.SERVICE_TYPE,
            udcd14.VQ_POP;




      pac_log.info(pac_name => $$PLSQL_UNIT, procedure_name => 'PRO_DAY_AGG', message => 'FIM DA PROCEDURE');

   EXCEPTION
      WHEN no_data_found THEN
         pac_log.warn(pac_name => $$PLSQL_UNIT, procedure_name => 'PRO_DAY_AGG', message => 'NAO EXISTEM DADOS A SEREM AGREGADOS');
      WHEN storage_error THEN
         pac_log.error(pac_name => $$PLSQL_UNIT, procedure_name => 'PRO_DAY_AGG', message => 'SEM MEMORIA DISPONIVEL OU MEMORIA CORROMPIDA', code => SQLCODE, errm => SQLERRM);
         RAISE;
      WHEN dup_val_on_index THEN
         pac_log.error(pac_name => $$PLSQL_UNIT, procedure_name => 'PRO_DAY_AGG', message => 'TENTATIVA DE INSERCAO DE DADOS COM CHAVE DUPLICADA', code => SQLCODE, errm => SQLERRM);
         RAISE;

  END pro_day_agg;


-- **************************************************************************************************
--  pro_rac_day_agg
-- **************************************************************************************************
PROCEDURE pro_rac_day_agg
   (
      p_start_range   IN NUMBER
     ,p_end_range     IN NUMBER
   ) IS

      v_min_std NUMBER := p_start_range;
      v_max_std NUMBER := p_end_range;

  BEGIN

      pac_log.info(pac_name => $$PLSQL_UNIT, procedure_name => 'PRO_RAC_DAY_AGG', message => 'INICIO DA PROCEDURE');

       insert into AG2_RAC_CONSOLIDADO(
               "DATE_TIME_KEY",
               "QUEUE_KEY",
               --ADICIONANDO NOME DA FILA IGUAL O RELATORIO ANALITICO POR TERMINAL
               "QUEUE_NAME",
               "OFERECIDAS",
               "DISTRIBUIDAS",
               "TEMPO_FILA_AGENTE",
               "VERBALIZACOES",
               "INCENTIVOS",
               "VERB_INCEN",
               "REGRAS_FIXAS",
               "REGRAS_PARAMETRIZADAS",
               "FALHAS",
               "VERBALIZADO_SIM",
               "VERBALIZADO_NAO",
               "VERBALIZADO_SEM_RESPOSTA",
               "VERBALIZADO_FALHA",
               "QTDE_ABANDONADO",
               "TEMPO_FILA_ABN",
               "RAC_ABN",
               "INCENTIVO_ABN",
               "INCENTIVO_ACEITE_ABN",
               "INCENTIVO_N_ACEITE_ABN",
               "REGRAS_PARAMETRIZAVEIS_ABN",
               "REGRAS_FIXAS_ABN",
               "FALHA_INCENTIVO_RAC_ABN"
              )
         SELECT
            dt.date_time_day_key DATE_TIME_KEY,
            /*
            CASE WHEN ira.QUEUE_RESOURCE_KEY is null
                  THEN msf.resource_key
                  ELSE ira.QUEUE_RESOURCE_KEY
            END
            */
            CASE WHEN ira.queue_key is null or PAC_RAC.get_queue_key(iudc.VQ_RAC_IM8) = -2 THEN
               msf.resource_key
               ELSE PAC_RAC.get_queue_key(iudc.VQ_RAC_IM8)
            END
             QUEUE_KEY,
            CASE WHEN r.RESOURCE_NAME = 'VQ_RAC_SMS' and iudc.VQ_RAC_IM8 is not null THEN
            iudc.VQ_RAC_IM8 else r.RESOURCE_NAME
            END QUEUE_NAME,

            SUM(CASE
                  WHEN (mt.is_online = 1 AND NOT (td2.resource_role_code = 'RECEIVEDCONSULT' AND (td2.technical_result_code IN ('ABANDONED', 'CLEARED') OR td2.result_reason_code = 'UNSPECIFIED')) AND
                        (NOT ((coalesce(irf.customer_handle_count, 1) + coalesce(irf.customer_ring_count, 1) = 0 AND it.interaction_subtype_code <> 'INTERNALCOLLABORATIONREPLY')) OR
                        td2.result_reason_code = 'ABANDONEDWHILEQUEUED'))
                        OR (mt.is_online = 0 AND
                        it.interaction_subtype_code NOT IN ('INTERNALCOLLABORATIONINVITE', 'INTERNALCOLLABORATIONREPLY', 'INBOUNDCOLLABORATIONREPLY', 'OUTBOUNDCOLLABORATIONINVITE')) THEN
                     1
                  ELSE
                     0
               END) AS OFERECIDAS,
            count(distinct iudc.account_number_ivr) AS DISTRIBUIDAS,
            SUM(CASE
                  WHEN (mt.is_online = 1 AND NOT ((coalesce(irf.customer_handle_count, 1) + coalesce(irf.customer_ring_count, 1) = 0 AND it.interaction_subtype_code <> 'INTERNALCOLLABORATIONREPLY')) AND
                        NOT (td2.resource_role_code = 'RECEIVEDCONSULT' AND td2.result_reason_code = 'UNSPECIFIED') AND td2.technical_result_code = 'DIVERTED')
                        OR (mt.is_online = 0 AND
                        it.interaction_subtype_code NOT IN ('INTERNALCOLLABORATIONINVITE', 'INTERNALCOLLABORATIONREPLY', 'INBOUNDCOLLABORATIONREPLY', 'OUTBOUNDCOLLABORATIONINVITE') AND
                        td2.technical_result_code = 'DIVERTED') THEN
                     msf.mediation_duration
                  ELSE
                     0
               END) AS TEMPO_FILA_AGENTE,
            SUM(CASE WHEN (udcd13.u_rac_tipo ='1' and      udcd13.s_rac_cod_que in ('RAC_1', 'RAC_3') and udcd13.s_rac_cod_abn ='NULL') and udcd13.u_rac_tipo_regra not in ('ATF', 'Parametrizada')
               THEN 1 else 0
               END) AS VERBALIZACOES,
            SUM(CASE WHEN ((udcd13.u_rac_tipo ='2' and udcd13.u_rac_tipo_regra <>'ATF') or udcd13.s_rac_cod_que = 'RAC_4') and udcd13.u_rac_tipo_regra not in ('ATF', 'Parametrizada')
               THEN 1 else 0
               END) AS INCENTIVOS,

/*CALCULO VERBALIZAÇÃO + INCENTIVO IGUAL AO CALCULO DO RELATÓRIO POR TERMINAL* 17/09 DURANTE HOMOLOGAÇÃO *ROBSON*/

            SUM(CASE WHEN udcd13.u_rac_tipo ='3' and udcd13.s_rac_cod_que in ('RAC_1', 'RAC_3') and udcd13.s_rac_cod_abn in ('NULL', 'RAC_2')
               THEN 1 else 0
               END) AS VERBALIZACOES_INCENTIVO,

/*           CALCULO ANTIGO VERB_INCENTIVO ALTERADO 17/09 DURANTE HOMOLOGAÇÃO *ROBSON*

SUM(CASE WHEN (udcd13.u_rac_tipo ='3' and      udcd13.s_rac_cod_que in ('RAC_1', 'RAC_3') and udcd13.s_rac_cod_abn ='NULL') and udcd13.u_rac_tipo_regra not in ('ATF', 'Parametrizada')
               THEN 1 else 0
               END) AS VERB_INCEN,*/

            SUM(CASE WHEN substr(udcd13.u_rac_tipo_regra,1,4)='Fixa'
               THEN 1 else 0
               END) AS REGRAS_FIXAS,
            SUM(CASE WHEN udcd13.u_rac_tipo_regra in 'Parametrizada'
               THEN 1 else 0
               END) AS REGRAS_PARAMETRIZADAS,
            SUM(CASE WHEN (udcd13.u_rac_tipo_regra in 'Erro_Sistemico' and udcd13.u_rac_tipo in ('99','404','999'))
               THEN 1 else 0
               END) AS FALHAS,
            SUM(CASE WHEN udcd13.s_rac_op_sms = '1'
               THEN 1 else 0
               END) AS VERBALIZADO_SIM,
            SUM(CASE WHEN udcd13.s_rac_op_sms = '2'
               THEN 1 else 0
               END) AS VERBALIZADO_NAO,
            SUM(CASE WHEN udcd13.s_rac_op_sms = 'timeout'
               THEN 1 else 0
               END) AS VERBALIZADO_SEM_RESPOSTA,
            SUM(CASE WHEN udcd13.s_rac_op_sms = 'erro'
               THEN 1 else 0
               END) AS VERBALIZADO_FALHA,
            SUM(CASE
                  WHEN td2.resource_role_code <> 'RECEIVEDCONSULT'
                        AND it.interaction_subtype_code NOT IN ('INTERNALCOLLABORATIONINVITE', 'INTERNALCOLLABORATIONREPLY', 'INBOUNDCOLLABORATIONREPLY', 'OUTBOUNDCOLLABORATIONINVITE')
                        AND td2.technical_result_code IN ('ABANDONED', 'CUSTOMERABANDONED') THEN
                     1
                  ELSE
                     0
               END) AS QTDE_ABANDONADO,
            SUM(CASE
                  WHEN td2.resource_role_code <> 'RECEIVEDCONSULT'
                        AND it.interaction_subtype_code NOT IN ('INTERNALCOLLABORATIONINVITE', 'INTERNALCOLLABORATIONREPLY', 'INBOUNDCOLLABORATIONREPLY', 'OUTBOUNDCOLLABORATIONINVITE')
                        AND td2.technical_result_code IN ('ABANDONED', 'CUSTOMERABANDONED') THEN
                     msf.mediation_duration
                  ELSE
                     0
               END) AS TEMPO_FILA_ABN,
            SUM(CASE
                  WHEN td2.resource_role_code <> 'RECEIVEDCONSULT'
                        AND it.interaction_subtype_code NOT IN ('INTERNALCOLLABORATIONINVITE', 'INTERNALCOLLABORATIONREPLY', 'INBOUNDCOLLABORATIONREPLY', 'OUTBOUNDCOLLABORATIONINVITE')
                        AND td2.technical_result_code IN ('ABANDONED', 'CUSTOMERABANDONED')
                        AND udcd13.u_rac_tipo <> 'none' THEN
                     1
                  ELSE
                     0
               END) AS RAC_ABN,
           0 AS INCENTIVO_ABN,
           0 AS INCENTIVO_ACEITE_ABN,
           0 AS INCENTIVO_ACEITE_ABN,
           0 REGRAS_PARAMETRIZAVEIS_ABN,
           0 AS REGRAS_FIXAS_ABN,
           0 AS FALHA_INCENTIVO_RAC_ABN
         FROM gim81.mediation_segment_fact msf
         inner join gim81.date_time dt on dt.date_time_key = msf.start_date_time_key and dt.date_time_day_key = 1567998000

         INNER JOIN gim81.media_type mt ON (mt.media_type_key = msf.media_type_key AND mt.media_type_key = 1)
         INNER JOIN gim81.interaction_type it ON (it.interaction_type_key = msf.interaction_type_key)
         INNER JOIN gim81.resource_ r ON (r.resource_key = msf.resource_key AND r.resource_type_code = 'QUEUE' AND r.resource_subtype <> 'ACDQueue')
         INNER JOIN alu_msf_to_irf msfirf ON (msfirf.mediation_segment_id = msf.mediation_segment_id)
         INNER JOIN irf_user_data_keys irfud ON (msfirf.interaction_resource_id = irfud.interaction_resource_id )
         INNER JOIN gim81.irf_user_data_cust_1 iudc on iudc.interaction_resource_id = msfirf.interaction_resource_id
         INNER JOIN  gim81.user_data_cust_dim_13 udcd13 on irfud.custom_key_13 = udcd13.id
         LEFT OUTER JOIN gim81.interaction_resource_fact irf ON (msf.target_ixn_resource_id = irf.interaction_resource_id  and
                                                             irf.resource_key IN (SELECT r_.resource_key
                                                                                   FROM   resource_ r_
                                                                                   WHERE  r_.resource_type_code IN ('AGENT', 'QUEUE', 'OTHER')))
         LEFT OUTER JOIN VW_ITX_RAC_ANALITICO ira ON ira.IRF_USER_DATA_CUST_1_KEY = irf.interaction_resource_id

         LEFT OUTER JOIN gim81.resource_ r2 ON (irf.resource_key = r2.resource_key)

         LEFT OUTER JOIN technical_descriptor td2 ON (irf.technical_descriptor_key = td2.technical_descriptor_key)

         where 1=1
         and r.resource_name not in ('VQ_CallCenter','NONE')
       and r.resource_subtype = 'VirtualQueue'
and r.switch_name = 'sip_vq_switch'
and r.resource_name not like 'VQ_LL%' and r.resource_name not like 'VQ_FIXA%' and r.resource_name not like 'VQ_URA%' and r.resource_name not like 'VQ_DEALER%'

            and it.interaction_type_code = 'INBOUND'
            AND NOT (it.interaction_subtype_code IN ('INBOUNDCOLLABORATIONREPLY') AND irf.interaction_resource_id IS NULL)
            AND NOT (it.interaction_subtype_code IN ('INBOUNDCOLLABORATIONREPLY') AND irf.cons_rcv_talk_duration = 0)
 --and msf.resource_key ='771365'
          group by dt.date_time_day_key,
          /*
            CASE WHEN ira.QUEUE_RESOURCE_KEY is null
                  THEN msf.resource_key
                  ELSE ira.QUEUE_RESOURCE_KEY
            END
            */
            CASE WHEN ira.queue_key is null or PAC_RAC.get_queue_key(iudc.VQ_RAC_IM8) = -2 THEN
               msf.resource_key
               ELSE PAC_RAC.get_queue_key(iudc.VQ_RAC_IM8)
            END,
           CASE WHEN r.RESOURCE_NAME = 'VQ_RAC_SMS' and iudc.VQ_RAC_IM8 is not null THEN
            iudc.VQ_RAC_IM8 else r.RESOURCE_NAME
            END
            ;

      pac_log.info(pac_name => $$PLSQL_UNIT, procedure_name => 'PRO_RAC_DAY_AGG', message => 'FIM DA PROCEDURE');

   EXCEPTION
      WHEN no_data_found THEN
         pac_log.warn(pac_name => $$PLSQL_UNIT, procedure_name => 'PRO_RAC_DAY_AGG', message => 'NAO EXISTEM DADOS A SEREM AGREGADOS');
      WHEN storage_error THEN
         pac_log.error(pac_name => $$PLSQL_UNIT, procedure_name => 'PRO_RAC_DAY_AGG', message => 'SEM MEMORIA DISPONIVEL OU MEMORIA CORROMPIDA', code => SQLCODE, errm => SQLERRM);
         RAISE;
      WHEN dup_val_on_index THEN
         pac_log.error(pac_name => $$PLSQL_UNIT, procedure_name => 'PRO_RAC_DAY_AGG', message => 'TENTATIVA DE INSERCAO DE DADOS COM CHAVE DUPLICADA', code => SQLCODE, errm => SQLERRM);
         RAISE;

  END pro_rac_day_agg;



-- **************************************************************************************************
--  pro_rac_tmp
-- **************************************************************************************************
PROCEDURE pro_rac_tmp
   (
      p_start_range   IN NUMBER
     ,p_end_range     IN NUMBER
   ) IS

      v_min_std NUMBER := p_start_range;
      v_max_std NUMBER := p_end_range;

  BEGIN

      delete from TMP_RAC;

      pac_log.info(pac_name => $$PLSQL_UNIT, procedure_name => 'PRO_RAC_TMP', message => 'INICIO DA PROCEDURE');

       insert into TMP_RAC(
               "INTERACTION_RESOURCE_ID"
              )
         select
                     t.interaction_resource_id
                  from(
                     select
                           irf.interaction_id interaction_id,
                           irf.start_date_time_key,
                           irf.interaction_resource_id interaction_resource_id,
                           ROW_NUMBER() OVER(
                              PARTITION BY irf.interaction_id
                              ORDER BY
                                 irf.start_date_time_key
                           ) AS row_num
                     from gim81.interaction_resource_fact irf
                        inner join gim81.date_time dt on dt.date_time_key = irf.start_date_time_key and dt.date_time_key BETWEEN v_min_std AND (v_max_std-1)
                        inner join gim81.irf_user_data_keys iudk on iudk.interaction_resource_id = irf.interaction_resource_id and iudk.start_date_time_key = irf.start_date_time_key and iudk.start_date_time_key BETWEEN v_min_std AND (v_max_std-1)
                        inner join gim81.user_data_cust_dim_14 udcd14 on iudk.custom_key_14 = udcd14.id
                     where 1=1
                        and udcd14.NOME_SUBROTINA = 'sub_rac_v1'
                  ) t
                  where t.row_num = 1;

      pac_log.info(pac_name => $$PLSQL_UNIT, procedure_name => 'PRO_RAC_TMP', message => 'FIM DA PROCEDURE');

   EXCEPTION
      WHEN no_data_found THEN
         pac_log.warn(pac_name => $$PLSQL_UNIT, procedure_name => 'PRO_RAC_TMP', message => 'NAO EXISTEM DADOS A SEREM AGREGADOS');
      WHEN storage_error THEN
         pac_log.error(pac_name => $$PLSQL_UNIT, procedure_name => 'PRO_RAC_TMP', message => 'SEM MEMORIA DISPONIVEL OU MEMORIA CORROMPIDA', code => SQLCODE, errm => SQLERRM);
         RAISE;
      WHEN dup_val_on_index THEN
         pac_log.error(pac_name => $$PLSQL_UNIT, procedure_name => 'PRO_RAC_TMP', message => 'TENTATIVA DE INSERCAO DE DADOS COM CHAVE DUPLICADA', code => SQLCODE, errm => SQLERRM);
         RAISE;

  END pro_rac_tmp;



END pac_rac;
