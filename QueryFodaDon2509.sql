        
     select   
              T1.dtkey,
               T1.res_key,
               qname qname,
               T2.OFERECIDAS as OFERECIDAS,
               T1.distribuidas DISTRIBUIDAS,
               T2.TEMPO_FILA AS TEMPO_FILA,
               T1.VERBALIZACOES,
               T1.INCENTIVOS,
               T1.VERB_INCEN,
               T1.REGRAS_FIXAS,              
               T1.REGRAS_PARAMETRIZADAS,
               T1.FALHAS,                             
               T1.vERBALIZADO_SIM, 
               T1.VERBALIZADO_NAO,      
               T1.VERBALIZADO_SEM_RESPOSTA,   
               T1.VERBALIZADO_FALHA,
              T2.ABANDONADAS AS ABANDONADAS,
               T2.TEMPO_ABANDONAS AS TEMPO_ABANDONAS,
               T2.RAC_ABANDONADAS

               

   from     
               (   
   SELECT
                   dt.DATE_TIME_day_KEY dtkey,
                   t.queue_key res_key,
                   '' qname,
                   count(*) DISTRIBUIDAS,
                   sum(case when T.Tipo_Incentivo = 'Verbalizacao' then 1 else 0 end) VERBALIZACOES,
                   sum(case when T.Tipo_Incentivo = 'Incentivo' then 1 else 0 end) INCENTIVOS,
                   sum(case when T.Tipo_Incentivo = 'Verbalizacao+Incentivo' then 1 else 0 end) VERB_INCEN,
                   sum(case when T.Tipo_Incentivo like 'Fixa_%' then 1 else 0 end) REGRAS_FIXAS,              
                   sum(case when T.Tipo_Incentivo = 'Parametrizada' or T.Tipo_Incentivo = 'ATF'  then 1 else 0 end) REGRAS_PARAMETRIZADAS,
                   sum(case when T.Tipo_Incentivo = 'parametros invalidos' then 1 else 0 end) FALHAS,                             
                   sum(case when T.aceite = 'Aceite' then 1 else 0 end) VERBALIZADO_SIM, 
                   sum(case when T.aceite = 'Não aceite' then 1 else 0 end) VERBALIZADO_NAO,      
                   sum(case when T.aceite = 'Sem resposta' then 1 else 0 end) VERBALIZADO_SEM_RESPOSTA,   
                   sum(case when T.aceite = 'Falha  - Telefonia' then 1 else 0 end) VERBALIZADO_FALHA
                             
               from VW_ITX_RAC_ANALITICO T
                    inner join date_time dt on dt.date_time_key = t.date_time_key and dt.date_time_day_key = (select distinct date_time_day_key from date_time where label_yyyy_mm_dd = '2019-09-10')
               group by
                     dt.DATE_TIME_dAY_KEY,
                     t.queue_key
               )T1
               
               inner join 
               
               (select 
                       dt2.DATE_TIME_day_KEY dtkey,
                       q.resource_key res_key,
                       sum(q.entered) as OFERECIDAS,
                       sum(q.distributed_time) AS TEMPO_FILA,             
                      sum(q.abandoned) AS ABANDONADAS,
                       sum(q.abandoned_time) AS TEMPO_ABANDONAS,
                       sum (case when ud13.u_rac_tipo <> 'none' then q.abandoned  else 0 end) AS RAC_ABANDONADAS
               from ag2_queue_day Q 
                    inner join date_time dt2 on dt2.date_time_key = Q.date_time_key and dt2.date_time_day_key = (select distinct date_time_day_key from date_time dt where dt.label_yyyy_mm_dd = '2019-09-10')
                    inner join gim81.user_data_cust_dim_13 ud13 on ud13.id = q.user_data_key13
                    inner join resource_ r on r.resource_key = q.resource_key
               group by
                     dt2.DATE_TIME_dAY_KEY,
                     Q.resource_key
                )T2
               
               on T1.DtKEY = T2.DtKEY and T1.res_key = T2.res_key
 
 
 --(select date_time_key,resource_key, user_data_key13,sum(qu.entered),sum(qu.distributed_time),sum(qu.abandoned),sum(qu.abandoned_time) from AG2_QUEUE_DAY qu  group by date_time_key,resource_key, user_data_key13)